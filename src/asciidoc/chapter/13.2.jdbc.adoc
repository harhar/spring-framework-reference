[[jdbc-packages]]
==== 包结构
Spring 框架的JDBC抽象层由四个不同的包组成，即 `core`、 `datasource`、 `object` 和 `support`。

`org.springframework.jdbc.core` 这个包里包含了 `JdbcTemplate` 类、该类的回调接口，以及其他相关类。
子包 `org.springframework.jdbc.core.simple` 包含`SimpleJdbcInsert` 和 `SimpleJdbcCall` 类。
另一个子包 `org.springframework.jdbc.core.namedparam` 包含 `NamedParameterJdbcTemplate`
 类及其相关支持类。详情参见<<jdbc-core>>、<<jdbc-advanced-jdbc>>、和 <<jdbc-simple-jdbc>>。

`org.springframework.jdbc.datasource` 这个包里包含了一个简化`DataSource` 访问的工具类及`DataSource`的简化实现类。这些实现使得在Java EE容器外不用修改代码即可测试和运行我们的JDBC代码。这个包中还有一个名为`org.springfamework.jdbc.datasource.embedded`的子包，该子包为使用如HSQL和H2这样的Java数据库引擎创建内存数据库提供了支持。
详情参见 <<jdbc-connections>> 和 <<jdbc-embedded-database-support>>。


`org.springframework.jdbc.object`这个包包含了代表RDBMS中查询，修改和将存储过程作为可复用的线程安全对象存储的类。  详情参见<<jdbc-object>>.尽管查询返回的对象必然是“断开”了数据库的，这种方式是仍以JDO规范建模的。这种更高层级的JDBC抽象依赖于`org.springframework.jdbc.core`包中更低层级的JDBC抽象。

`org.springframework.jdbc.support`这个包提供了`SQLException` 转换功能和一些相关工具类。在JDBC执行过程中抛出的异常被转换为`org.springframework.dao`包中定义的异常。换句话说，使用了Sping JDBC抽象层的代码就不需要对JDBC或某种数据库特有的错误进行处理。所有被转换后的异常都是非受检异常--这给了我们选择的能力，使得在捕获那些可以被恢复的异常的同时允许其他异常被传递给调用者。详情请见<<jdbc-SQLExceptionTranslator>>。




[[jdbc-core]]
=== 使用JDBC核心类控制基本的JDBC的运行过程和错误处理

